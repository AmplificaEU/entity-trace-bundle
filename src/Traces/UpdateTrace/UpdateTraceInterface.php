<?php

namespace Fusely\EntityTraceBundle\Traces\UpdateTrace;

use App\Entity\User;
use App\Entity\Company;
use DateTimeInterface;

interface UpdateTraceInterface
{
    public function setUpdatedAt(?DateTimeInterface $UpdatedAt);
    public function getUpdatedAt(): ?DateTimeInterface;

    public function setUpdatedBy(?int $userId);
    public function getUpdatedBy(): ?int;

    public function setUpdatedAs(?string $userRole);
    public function getUpdatedAs(): ?string;

    public function setUpdatedByCompany(?int $companyId);
    public function getUpdatedByCompany(): ?int;
}
