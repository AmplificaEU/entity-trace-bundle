<?php

namespace Fusely\EntityTraceBundle\Traces\UpdateTrace;

use App\Entity\Company;
use App\Entity\User;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait UpdateTraceTrait
{
    #[ORM\Column(
        type: 'datetime',
        nullable: true
    )]
    #[Groups(['get'])]
    private ?DateTimeInterface $updatedAt;

    /**
     * User that updated entity
     */
    #[ORM\Column(
        type: 'integer',
        nullable: true,
        options: ['unsigned' => true]
    )]
    #[Groups(['get'])]
    private ?int $updatedBy;

    /**
     * ROLE used on the creation time
     */
    #[ORM\Column(
        type: 'string',
        length: 255,
        nullable: true
    )]
    #[Groups(['get'])]
    private ?string $updatedAs;

    /**
     * Company that updated entity
     */
    #[ORM\Column(
        type: 'integer',
        nullable: true,
        options: ['unsigned' => true]
    )]
    #[Groups(['get'])]
    private ?int $updatedByCompany;


    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt(?DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }


    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }


    public function getUpdatedAs(): ?string
    {
        return $this->updatedAs;
    }

    public function setUpdatedAs(?string $updatedAs): void
    {
        $this->updatedAs = $updatedAs;
    }


    public function getUpdatedByCompany(): ?int
    {
        return $this->updatedByCompany;
    }

    public function setUpdatedByCompany(?int $updatedByCompany): void
    {
        $this->updatedByCompany = $updatedByCompany;
    }
}
