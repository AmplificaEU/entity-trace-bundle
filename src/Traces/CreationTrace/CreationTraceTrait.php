<?php

namespace Fusely\EntityTraceBundle\Traces\CreationTrace;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\User;
use App\Entity\Company;
use DateTimeInterface;

trait CreationTraceTrait
{
    #[ORM\Column(
        type: 'datetime',
        nullable: false,
        options: ['default' => 'CURRENT_TIMESTAMP']
    )]
    #[Groups(['get'])]
    private ?DateTimeInterface $createdAt;

    /**
     * User that created entity
     */
    #[ORM\Column(
        type: 'integer',
        nullable: true,
        options: ['unsigned' => true]
    )]
    #[Groups(['get'])]
    private ?int $createdBy;

    /**
     * ROLE used on the creation time
     */
    #[ORM\Column(
        type: 'string',
        nullable: true,
        length: 255
    )]
    #[Groups(['get'])]
    private ?string $createdAs;

    /**
     * Company that created entity
     */
    #[ORM\Column(
        type: 'integer',
        nullable: true,
        options: ['unsigned' => true]
    )]
    #[Groups(['get'])]
    private ?int $createdByCompany;


    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }


    public function setCreatedAt(?DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): void
    {
        $this->createdBy = $createdBy;
    }


    public function getCreatedAs(): ?string
    {
        return $this->createdAs;
    }

    public function setCreatedAs(?string $createdAs): void
    {
        $this->createdAs = $createdAs;
    }


    public function getCreatedByCompany(): ?int
    {
        return $this->createdByCompany;
    }

    public function setCreatedByCompany(?int $createdByCompany): void
    {
        $this->createdByCompany = $createdByCompany;
    }
}
