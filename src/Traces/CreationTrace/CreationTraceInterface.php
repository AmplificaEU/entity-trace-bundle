<?php

namespace Fusely\EntityTraceBundle\Traces\CreationTrace;

use DateTimeInterface;

interface CreationTraceInterface
{
    public function setCreatedAt(?DateTimeInterface $createdAt);
    public function getCreatedAt(): ?DateTimeInterface;

    public function setCreatedBy(?int $userId);
    public function getCreatedBy(): ?int;

    public function setCreatedAs(?string $userRole);
    public function getCreatedAs(): ?string;

    public function setCreatedByCompany(?int $companyId);
    public function getCreatedByCompany(): ?int;
}
