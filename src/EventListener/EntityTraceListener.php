<?php
// src/EventListener/DatabaseActivitySubscriber.php
namespace Fusely\EntityTraceBundle\EventListener;

use Fusely\AmplificaEnumBundle\Enum\UserRole;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Fusely\EntityTraceBundle\Traces\CreationTrace\CreationTraceInterface;
use Fusely\EntityTraceBundle\Traces\UpdateTrace\UpdateTraceInterface;
use Symfony\Component\Security\Core\Security;


class EntityTraceListener implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    // this method can only return the event names; you cannot define a
    // custom method name to execute when each event triggers
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preRemove,
            Events::preUpdate,
        ];
    }

    // callback methods must be called exactly like the events they listen to;
    // they receive an argument of type LifecycleEventArgs, which gives you access
    // to both the entity object of the event and the entity manager itself
    public function prePersist(LifecycleEventArgs $args): void
    {
        // Set the data
        $entity = $args->getObject();
        if ($entity instanceof CreationTraceInterface) {
            $this->handlePersist($entity);
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
//        $this->doTrace('remove', $args);
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if ($entity instanceof UpdateTraceInterface) {
            $this->handleUpdate($entity);
        }
    }

    public function handlePersist(CreationTraceInterface $entity): void
    {
        $entity->setCreatedAt(new \DateTime());
        
        $user = $this->security->getUser();
        if (!is_null($user)) {
            $companyId = $user->getAttribute('current_company') == 0 ? null : (int)$user->getAttribute('current_company');
            $entity->setCreatedBy((int)$user->getAttribute('amp_id'));
            $entity->setCreatedAs($user->getAttribute('current_role') ?? UserRole::USER);
            $entity->setCreatedByCompany($companyId);
        }
    }

    public function handleUpdate(UpdateTraceInterface $entity): void
    {
        $entity->setUpdatedAt(new \DateTime());

        $user = $this->security->getUser();
        if (!is_null($user)) {
            {
                $companyId = $user->getAttribute('current_company') == 0 ? null : (int)$user->getAttribute('current_company');
                $entity->setUpdatedBy((int)$user->getAttribute('amp_id'));
                $entity->setUpdatedAs($user->getAttribute('current_role') ?? UserRole::USER);
                $entity->setUpdatedByCompany($companyId);
            }
        }
    }
}